<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/', function () {
    return view('items.index');
});

Route::get('/film', function () {
    return view('film.index');
});

Route::get('/genre', function () {
    return view('genre.index');
});

Route::get('/about', function () {
    return view('about.index');
});


//CRUD Genre Table
    // 1. Create 
// Untuk Masuk ke Form Genre
Route::get('/genre/create', 'GenreController@create');
// Ini untuk mengirim data ke tabel genre
Route::post('/genre', 'GenreController@store'); 

// 2. Read
// Menamplkan Semua data tabel genres
Route::get('/genre', 'GenreController@index');
// Menampilkan data berdasarkan $id
Route::get('/genre/{id}', 'GenreController@show');

// 3. Update
// Masuk ke form, berdasarkan $id
Route::get('/genre/{id}/edit', 'GenreController@edit');
// untuk edit data berdasar id. Method PUT
Route::put('/genre/{id}', 'GenreController@update');

// 4. Delete
// Delete data berdasarkan id
Route::delete('/genre/{id}', 'GenreController@destroy');


//CRUD Cast Table
    // 1. Create 
// Untuk Masuk ke Form Cast
Route::get('/cast/create', 'CastController@create');
// Ini untuk mengirim data ke tabel Cast
Route::post('/cast', 'CastController@store'); 

// 2. Read
// Menamplkan Semua data tabel Cast
Route::get('/cast', 'CastController@index');
// Menampilkan data berdasarkan $id
Route::get('/cast/{id}', 'CastController@show');

// 3. Update
// Masuk ke form, berdasarkan $id
Route::get('/cast/{id}/edit', 'CastController@edit');
// untuk edit data berdasar id. Method PUT
Route::put('/cast/{id}', 'CastController@update');

// 4. Delete
// Delete data berdasarkan id
Route::delete('/cast/{id}', 'CastController@destroy');

//CRUD Film Tabel
Route::resource('film', 'FilmController');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
