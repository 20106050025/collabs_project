@extends('wagon.master')

@section('konten')


            <!-- ***** Main Banner Area Start ***** -->
            <div class="main-banner" id="top">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="right-content">
                                <div class="row">
                                    @forelse ($genre as $key => $item)
                                    <div class="col-lg-4">
                                        <div class="right-first-image">
                                            <div class="thumb">
                                                <div class="inner-content">
                                                    <h4>{{$item->nama}}</h4>
                                                    <span>More about {{$item->nama}}</span>
                                                </div>
                                                <div class="hover-content">
                                                    <div class="inner">
                                                        <h4>{{$item->nama}}</h4>
                                                        <p>Lorem ipsum dolor sit amet, conservisii ctetur adipiscing elit incid.</p>
                                                        <div class="main-border-button">
                                                            <form action="/genre/{{$item->id}}" method="POST">
                                                                @csrf
                                                                @method('delete')
                                                                <a href="/genre/{{$item->id}}">More</a>
                                                                <a href="/genre/{{$item->id}}/edit">Update</a>
                                                                <input type="submit" value="Delete">
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="{{asset('template/assets/images/4.png')}}">
                                            </div>
                                        </div>
                                    </div>
                                    @empty
                                    <h3>
                                        Tidak Ada Data di Halaman Ini Ini
                                    </h3>
                                @endforelse
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <a href="/genre/create" class="align-center btn btn-info btn-sm"> Tambah Genre</a>
                    </div>
                </div>
            </div>
            <!-- ***** Main Banner Area End ***** -->
    <br></br>
    <br></br>
@endsection