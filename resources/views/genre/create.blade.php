    <div>
        <h3 class="card-title">Form Input Genre</h3>
    </div>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <form action="/genre" method="POST">
        @csrf
        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" id="nama" placeholder="Masukan Nama" name="nama" value="{{old('nama')}}">
        </div>
        <div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>