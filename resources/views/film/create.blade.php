@extends('wagon.master')

@section('konten')
<br></br>
<br></br>
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Form Input Data Genre</h3>
    </div>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <form action="/film" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="judul">Judul</label>
                <input type="text" class="form-control" id="judul" placeholder="Masukan Judul" name="judul" value="{{old('judul')}}">
            </div>
            <div class="form-group">
                <label for="ringkasan">Ringkasan</label>
                <input type="text" class="form-control" id="ringkasan" placeholder="Masukan Ringkasan" name="ringkasan" value="{{old('ringkasan')}}">
            </div>
            <div class="form-group">
                <label for="tahun">Tahun Rilis</label>
                <input type="number" class="form-control" id="tahun" placeholder="Masukan Tahun" name="tahun" value="{{old('tahun')}}">
            </div>
            <div class="form-group">
                <label for="genre_id">Genre</label>
                <select name="genre_id" id="genre_id" class="form-control">
                    <option value="---Choose One---"></option>
                    @forelse ($genre as $item)
                        <option value="{{$item->id}}">{{$item->nama}}</option>
                    @empty
                        <option value="">Tidak ada Film</option>
                    @endforelse
                </select>
            </div>
            <div class="form-group">
                <label for="poster">Thumbnail</label>
                <input type="file" id="poster" name="poster" class="form-control">
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
    </form>
</div>
@endsection