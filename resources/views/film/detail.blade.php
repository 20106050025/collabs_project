@extends('wagon.master')

@section('konten')
<br></br>
<br></br>
<a href="/film" class="btn btn-info btn-sm mb-3">Back</a>
<section class="py-5">
    <div class="container px-4 px-lg-5 my-5">
        <div class="row gx-4 gx-lg-5 align-items-center">
            <div class="col-md-6"><img class="card-img-top mb-5 mb-md-0" src="{{asset('images/'. $film->poster)}}" alt="Ini Poster" /></div>
            <div class="col-md-6">
                <h1 class="display-5 fw-bolder">{{$film->judul}}</h1>
                <div class="fs-5 mb-5">
                    <span>{{$film->tahun}}</span>
                </div>
                <p class="lead">{{$film->ringkasan}}</p>
            </div>
        </div>
    </div>
</section>

@endsection