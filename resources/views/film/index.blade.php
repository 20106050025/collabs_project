@extends('wagon.master')

@section('konten')
            <!-- ***** Men Area Starts ***** -->
            <section class="section" id="men">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="section-heading">
                                <h2 class="text-center">List Films</h2>
                                <p class="text-center">Check it out your movies below.</p>
                                <a href="/film/create" class="btn btn-info btn-sm mb-3">Tambah Film</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="men-item-carousel">
                                <div class="owl-men-item owl-carousel">
                                    @forelse ($film as $item)
                                    <div class="item">
                                        <div class="thumb">
                                            <div class="hover-content">
                                                <form action="/film/{{$item->id}}" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                      <a href="/film/{{$item->id}}" class="btn btn-info btn-sm">Show More</a>
                                                      <a href="/film/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                                                      <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                                                </form>
                                            </div>
                                            <div style="height: 220px;">
                                                <img src="{{asset('images/'. $item->poster)}}" alt="">
                                            </div>
                                        </div>
                                        <div class="down-content">
                                            <h4>{{$item->judul}}</h4>
                                            <span>{{$item->tahun}}</span>
                                        </div>
                                    </div>
                                    @empty
                                        <h1>Tidak ada film</h1>
                                    @endforelse
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- ***** Men Area Ends ***** -->
@endsection