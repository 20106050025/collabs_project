@extends('wagon.master')

@section('konten')
    
<div>
    <h3 class="card-title">Form Input Cast</h3>
</div>
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="subscribe">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-heading text-center">
                    <h2>Form Input Data Cast</h2>
                    <span>Please input correct data cast in the from.</span>
                </div>
                <form id="subscribe" action="/cast" method="POST">
                    @csrf
                    <div class="row">
                      <div class="col-lg-5">
                        <fieldset>
                            <label for="nama">Nama</label>
                            <input type="text" class="form-control" id="nama" placeholder="Masukan Nama" name="nama" value="{{old('nama')}}">
                        </fieldset>
                      </div>
                      <div class="col-lg-2">
                        <fieldset>
                            <label for="umur">Umur</label>
                            <input type="number" class="form-control" id="umur" placeholder="Masukan Umur" name="umur" value="{{old('umur')}}">
                        </fieldset>
                      </div>
                      <div class="col-lg-5">
                        <fieldset>
                            <label for="bio">Biografi</label>
                            <input type="text" class="form-control" id="bio" placeholder="Masukan Biografi" name="bio" value="{{old('bio')}}">
                        </fieldset>
                      </div>
                      <div class="col-lg-1">
                        <fieldset>
                          <button type="submit" id="form-submit" class="main-dark-button"><i class="fa fa-paper-plane"></i></button>
                        </fieldset>
                      </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection