@extends('wagon.master')

@section('konten')
<br></br>
<br></br>
<br></br>
    <a href="/cast" class="btn btn-info btn-sm mb-3">Back</a>
    <div class="card">
        <div class="card-body">
          <h5 class="card-title text-primary text-center">{{$cast->nama}}</h5>
          <h6 class="card-subtitle mb-2 text-muted text-center">Umur :{{$cast->umur}} tahun</h6>
          <p class="card-text text-center">Biografi : {{$cast->bio}}</p>
        </div>
      </div>
@endsection