<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="under-footer">
                    <p>Kelompok 7 &#10084; Love from Indonesia. 
                </div>
            </div>
        </div>
    </div>
</footer>


<!-- jQuery -->
<script src=" {{asset('template/assets/js/jquery-2.1.0.min.js')}} "></script>

<!-- Bootstrap -->
<script src=" {{asset('template/assets/js/popper.js')}} "></script>
<script src=" {{asset('template/assets/js/bootstrap.min.js')}} "></script>

<!-- Plugins -->
<script src=" {{asset('template/assets/js/owl-carousel.js')}} "></script>
<script src=" {{asset('template/assets/js/accordions.js')}} "></script>
<script src=" {{asset('template/assets/js/datepicker.js')}} "></script>
<script src=" {{asset('template/assets/js/scrollreveal.min.js')}} "></script>
<script src=" {{asset('template/assets/js/waypoints.min.js')}} "></script>
<script src=" {{asset('template/assets/js/jquery.counterup.min.js')}} "></script>
<script src=" {{asset('template/assets/js/imgfix.min.js')}} "></script> 
<script src=" {{asset('template/assets/js/slick.js')}} "></script> 
<script src=" {{asset('template/assets/js/lightbox.js')}} "></script> 
<script src=" {{asset('template/assets/js/isotope.js')}} "></script> 
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
<!-- Global Init -->
<script src=" {{asset('template/assets/js/custom.js')}} "></script>

<script>

    $(function() {
        var selectedClass = "";
        $("p").click(function(){
        selectedClass = $(this).attr("data-rel");
        $("#portfolio").fadeTo(50, 0.1);
            $("#portfolio div").not("."+selectedClass).fadeOut();
        setTimeout(function() {
          $("."+selectedClass).fadeIn();
          $("#portfolio").fadeTo(50, 1);
        }, 500);
            
        });
    });

</script>
