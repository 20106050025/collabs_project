<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GenreController extends Controller
{
    public function __construct(){
        $this->middleware('auth')->except(['index']);
    }
    
    public function create()
    {
        return view('genre.create');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate(
            [
                'nama' => 'required'
            ],
            [
                'nama.required' => 'Sebentar Kak, Nama tidak boleh kosong'
            ]
        );

        DB::table('genre')->insert(
            [
                'nama' => $request['nama']
            ]
        );
        return redirect('/genre');
    }

    public function index()
    {
        $genre = DB::table('genre')->get();
        return view('genre.index', compact('genre'));
    }

    public function show($id)
    {
        $genre = DB::table('genre')->where('id', $id)->first();
        return view('genre.more', compact('genre'));
    }

    public function edit($id)
    {
        $genre = DB::table('genre')->where('id', $id)->first();
        return view('genre.update', compact('genre'));
    }

    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'nama' => 'required'
            ],
            [
                'nama.required' => 'Sebentar Kak, Nama tidak boleh kosong'
            ]
        );

        DB::table('genre')
        ->where('id', $id)
        ->update(
            [
            'nama' => $request['nama']
            ]
        );
        return redirect('/genre');
    }

    public function destroy($id)
    {
        DB::table('genre')->where('id', $id)->delete();
        return redirect('/genre');
    }
}
